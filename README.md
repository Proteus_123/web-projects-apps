# Hey!
Thanks for visiting this repository! Here you can find almost all of my web projects, including games and Paint-like program.
I update my projects regularly, but if you found a bug [DM me on Discord](https://discord.com/channels/@me/831489273172852786) or make a pull request.

If you want to check out my projects, download them and start playing. Remember, my products are licensed, so don't download like crazy and present it as your stuff. **It's not cool at all.**
I will make a file with instructions soon, but using the most my apps is quite easy, and titles are self-explanatory.
At this time, you can see following projects here:

 [✓] Photo Gallery 

 [✓] Paint-like (all 3 versions)

 [✓] Platformer game (still buggy)

 [✓] Weather and Time showing programs (shows stats only for Poznan/Poland, my city, I will upgrade it soon) 

 [✕] Well, i've got some more stuff to do, but i still want to learn new things! If you have a suggestion about the project i can start, send it to my Discord.

**I appreciate (and recommend) making pull requests and helping me in complicated parts of code!**